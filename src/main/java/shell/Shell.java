package shell;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Scanner;

import files.*;
import processes.*;
import virtualmemory.*;
import interpreter.*;
import memory.*;
import locks.*;
import cfs.*;


public class Shell
{
	Scanner scan = new Scanner(System.in);

	private void Read()
	{
	     scan.nextLine();
	}
	
	//lista dostepnych komend
	private static ArrayList<String> commands = new ArrayList<String>();
	//liczba argumentow dla kazdej komendy
	private static HashMap<String, Integer> argumentsNumber = new HashMap<String, Integer>();
	//opis kazdej komendy
	private static HashMap<String, String> commandsHelp = new HashMap<String, String>();

	public Interpreter inter = new Interpreter();
	//public Memory memory = new Memory();
	public PCB_management processes = new PCB_management();
	public FileSystem files = new FileSystem();
	public Locks locks = new Locks();
	public VirtualMemory vm = new VirtualMemory();
	public scheduler sche = new scheduler();
	public int x;

	private String directory = new String("");

	//sprawdza, czy PCB o podanej nazwie istnieje i go usuwa
	private boolean killPCB(String name)
	{
		for(int i=0; i<processes.processes_List.size(); i++) //**dodac nazwe obiektu klasy PCB_management
		{
			if(name.equals(processes.processes_List.get(i).getP_name()))	//**dodac nazwe obiektu klasy PCB_management
			{
				processes.kill(processes.processes_List.get(i)); //**dodac nazwe obiektu klasy PCB_management
				return true;
			}
		}
		return false;
	}

	//czysci konsole
	private static void clear()
	{
		try
		{
			if (System.getProperty("os.name").contains("Windows"))
				new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
			else
				Runtime.getRuntime().exec("clear");
		}
		catch (IOException | InterruptedException ex) {}
	}

	//lista komend, liczby argumentow, opisy
	static
	{
		commands.add("cp");
		argumentsNumber.put("cp",2);
		commandsHelp.put("cp","Usage: cp name... FILE...\nCreates a new process.\n");

		commands.add("ps");
		argumentsNumber.put("ps",0);
		commandsHelp.put("ps","Usage: ps\nViewing information related with the processes on a system.\n");

		commands.add("rm");
		argumentsNumber.put("rm",1);
		commandsHelp.put("rm","Usage: rm FILE...\nDeleting a FILE/CATALOG.\n");

		commands.add("ls");
		argumentsNumber.put("ls",0);
		commandsHelp.put("ls","Usage: ls\nLists directory contents of files and directories.\n");

		commands.add("lslocks");
		argumentsNumber.put("lslocks",0);
		commandsHelp.put("lslocks","Usage: lslocks\nList local system locks.\n");

		commands.add("more");
		argumentsNumber.put("more",1);
		commandsHelp.put("more","Usage: more FILE...\n\n");

		commands.add("kill");
		argumentsNumber.put("kill",1);
		commandsHelp.put("kill","Usage: kill name...\nTerminate processes manually.\n");

		commands.add("touch");
		argumentsNumber.put("touch",1);
		commandsHelp.put("touch","Usage: touch FILE...\nCreates a text file.\n");

		commands.add("free");
		argumentsNumber.put("free",0);
		commandsHelp.put("free","Usage: free\nShows the used and free space of RAM.\n");

		commands.add("help");
		argumentsNumber.put("help",0);
		commandsHelp.put("help","Usage: help\nDisplay information about builtin commands.\n");

		commands.add("clear");
		argumentsNumber.put("clear",0);
		commandsHelp.put("clear","Usage: clear\nClears the terminal screen.\n");

		commands.add("whoami");
		argumentsNumber.put("whoami",0);
		commandsHelp.put("whoami","Usage: whoami\nPrint the user name associated with the current effective user ID.\n");

		commands.add("shutdown");
		argumentsNumber.put("shutdown",0);
		commandsHelp.put("shutdown","Usage: shutdown\nShut down the system.\n");

		commands.add("echo");
		argumentsNumber.put("echo",100);
		commandsHelp.put("echo","Usage: echo text\necho text >> FILE...\necho text > FILE...\n\nDisplay line of text/string that are passed as an argument. Writes or appends text to file.\n");

		//commands.add("type");
		//argumentsNumber.put("type",1);
		//commandsHelp.put("type","Usage: type name\nShows the contents RAM cell.\n");

		commands.add("cd");
		argumentsNumber.put("cd",1);
		commandsHelp.put("cd","Usage: cd directory...\nUsed to change the current working directory.\n");

		commands.add("rmdir");
		argumentsNumber.put("rmdir",1);
		commandsHelp.put("rmdir","Usage: rmdir directory...\nRemove directories from the filesystem.\n");

		commands.add("mkdir");
		argumentsNumber.put("mkdir",1);
		commandsHelp.put("mkdir","Usage: mkdir directory...\nMake new directories.\n");
		
		commands.add("full");
		argumentsNumber.put("full",0);
		commandsHelp.put("full","Usage: full\nPrints all ram memory.\n");
		
		commands.add("run");
		argumentsNumber.put("run",0);
		commandsHelp.put("run","Usage: run\nPerforms all processes.\n");
		
		commands.add("df");
		argumentsNumber.put("df",0);
		commandsHelp.put("df","Usage: df\nShows the amount of disk space used and available.\n");
	}

	//sprawdzanie, czy wpisana komenda jest dostepna
	private boolean recognizeCommand(String command)
	{
		for(int i=0; i<commands.size(); i++)
			if(command.equals(commands.get(i))) return true;
		return false;
	}

	//sprawdzanie, czy wprowadzona liczba argumentow jest ok
	private boolean checkIfCorrect(ArrayList<String> command)
	{
		if("echo".equals(command.get(0)))
		{
			if(command.size()>=4 && (">>".equals(command.get(command.size()-2)) || ">".equals(command.get(command.size()-2))))
				return true;
			else if (command.size()>1) return true;
		}
		if("cd".equals(command.get(0)) && command.size()==1) return true;
		if(command.size()-1 == argumentsNumber.get(command.get(0)))
			return true;
		return false;
	}

	//wykonywanie komendy
	private void executeCommand(String command) throws IOException, FileException {
		ArrayList<String> commandParts = new ArrayList<String>();
		Matcher part = Pattern.compile("([A-z0-9]|[>-])+").matcher(command);
		while (part.find())
			commandParts.add(part.group());

		int size = commandParts.size();
		if(commandParts.size() != 0 && recognizeCommand(commandParts.get(0)))
		{
			if(commandParts.size()==2 && "--help".equals(commandParts.get(1)))
				System.out.printf("%s\n",commandsHelp.get(commandParts.get(0)));
			else if(checkIfCorrect(commandParts))
			{
				boolean check = true;
				ArrayList<String> text = new ArrayList<String>();
				ArrayList<ArrayList<String>> doubleText = new ArrayList<ArrayList<String>>();
				String singleText = new String();

				switch(commandParts.get(0))
				{
					//Tworzenie procesu
					case "cp":
						processes.new_process(commandParts.get(1),commandParts.get(2));
						if(check==false)
						{
							System.out.printf("bash: cp: (%s) - Can not create such process", commandParts.get(1));
							break;
						}
						break;
					case "run":
						do
						{
							sche.terminateOnce();
							Read();
						}while(!sche.readyProcesses.isEmpty());
							
						break;
					//Wypisywanie procesow
					case "ps":
						System.out.println("NAME\tID\tMODE\n");
						processes.WriteProcessesList();
						break;
					//Usuwanie procesu
					case "kill":
						check = killPCB(commandParts.get(1));
						if(check==false) System.out.printf("bash: kill: (%s) - There is no such process\n", commandParts.get(1));
						break;
					//wypisywanie zawartosci katalogu
					case "ls":
						files.listDir();
						break;
					//wypisywanie zamkow
					case "lslocks":
						doubleText = locks.lslocks();
						System.out.println("NAME\tMODE\n");
						for(int i=0; i<doubleText.size(); i++)
						{
							System.out.printf("%s\t",doubleText.get(0).get(i));
							System.out.printf("%s\n",doubleText.get(1).get(i));
						}
						break;
					//tworzenie pliku
					case "touch":
						check = files.createFile(commandParts.get(1)); //check
						if(check==false) System.out.printf("bash: touch: could not create file '%s': File exists\\n", commandParts.get(1));
						break;
					//usuwanie pliku
					case "rm":
						check = files.deleteFile(commandParts.get(1));
						if(check==false) System.out.printf("bash: rm: cannot be deleted '%s': There is no such file\n", commandParts.get(1));
						break;
					//wypisywanie zawartosci pliku
					case "more":
						x = files.open(commandParts.get(1));
						singleText = files.bytetoString(files.readfromFile(x,1024));
						System.out.println(singleText);
						if(x==-1) System.out.printf("more: cannot open '%s' for reading: There is no such file\n",commandParts.get(1));
						files.close(x);
						break;
					//wypisywanie stanu ramu
					case "free":
						byte[] ram = vm.Ram.getRawRAM();
						int fs = 0;
						for(Byte r: ram)
							if(r==0) fs++;
						int us= 256-fs;
						System.out.println("\ttotal\tused\tfree");
						System.out.printf("Mem:\t256\t%d\t%d\n", us, fs);
						break;
					//wypisywanie dostepnych komend
					case "help":
						for(int i=0, k=0; i<commands.size(); i++)
						{
							if(commands.get(i)=="cd" || commands.get(i)=="rmdir" || commands.get(i)=="mkdir")
								System.out.printf("%s directory",commands.get(i));
							else if(argumentsNumber.get(commands.get(i)) == 1)
								System.out.printf("%s FILE...",commands.get(i));
							else if(argumentsNumber.get(commands.get(i)) == 2)
								System.out.printf("%s name... FILE...",commands.get(i));
							else System.out.printf("%s",commands.get(i));

							if(k==1 || i==commands.size()-1)
							{
								System.out.print("\n");
								k=0;
							}
							else if(k==0)
							{
								System.out.print("\t");
								k=1;
							}
						}
						break;
					//czysci konsole
					case "clear":
						clear();
						break;
					//podaje nazwe uzytkownika
					case "whoami":
						System.out.println("root");
						break;
					//wylacza system
					case "shutdown":
						System.exit(0);
						break;
					//nadpisywanie lub zapisywanie w pliku
					case "echo":
						if(">>".equals(commandParts.get(commandParts.size()-2)))
						{
							String textAdd = "";
							for(int i=1; i<commandParts.size()-2; i++)
								textAdd += commandParts.get(i) + " ";
							check = files.appendFile(files.open(commandParts.get(commandParts.size()-1)), files.Stringtobyte(textAdd));
							if(check==false) System.out.printf("bash: echo: cannot be added '%s': There is no such file\n", commandParts.get(commandParts.size()-1));
							files.close(x);
						}
						else if(">".equals(commandParts.get(commandParts.size()-2)))
						{

							String textWrite = "";
							for(int i=1; i<commandParts.size()-2; i++)
								textWrite += commandParts.get(i) + " ";
							check = files.writetoFile(files.open(commandParts.get(commandParts.size()-1)), files.Stringtobyte(textWrite)); //**dodaæ nazwê obiektu klasy FileSystem
							if(check==false) System.out.printf("bash: echo: cannot be added '%s': There is no such file\n", commandParts.get(commandParts.size()-1));
							files.close(x);
						}
						else
						{
							for(int i=1; i<commandParts.size(); i++)
								System.out.printf("%s ",commandParts.get(i));
							System.out.print("\n");
						}
						break;
					//wypisuje zawartosc ramu
					/*case "type":
						Byte[] ram = memory.read(int processID, byte address);
						for(Byte r: ram)
							System.out.print(r+" ");
					break;*/
					case "cd":
						if(commandParts.size()==1) directory="";
						else
						{
							//check = files.changeDir(commandParts.get(1));
							if(check) directory = commandParts.get(1);
							else System.out.printf("bash: cd: '%s': There is no such directory\n", commandParts.get(1));
						}
						break;
					case "full":
						int i=0;
						byte[] Rram = vm.Ram.getRawRAM();
						for(Byte r: Rram)
						{
							i++;
							if(r==null) System.out.print("0 ");
							else System.out.print(r+" ");
							if(i==16)
							{
								System.out.print("\n");
								i=0;
							}
						}
						break;
					case "rmdir":
						check = files.deleteDir(commandParts.get(1));
						if(check==false) System.out.printf("bash: rm: cannot be deleted '%s': There is no such directory\n", commandParts.get(1));
						break;
					case "mkdir":
						check = files.createDirectory(commandParts.get(1));
						if(check==false) System.out.printf("bash: mkdir: could not create directory '%s': Directory exists\n", commandParts.get(1));
						break;
					case "df":
						files.disk.stan_dysku();
						break;
				}
			}
			else System.out.println("bash: wrong number of arguments");
		}
		else
		{
			if(commandParts.size() == 0)
			{
				//przeskakuje do nastepnej linii, bez info
				//System.out.println("No command given");
			}
			else
			{
				System.out.printf("bash: %s: command not found\n",commandParts.get(0));
			}
		}
	}

	public void boot () throws IOException, FileException {
		while(true)
		{
			Scanner input = new Scanner(System.in);
			System.out.printf("root@Linux:~%s$ ", directory);
			String command = input.nextLine();

			executeCommand(command);
		}
	}
}