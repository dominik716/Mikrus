package processes;
import files.*;
import virtualmemory.*;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

public class PCB_management {

	//pomaga nadawac ID
	int p_id_counter = 1;
	
	VirtualMemory vm= new VirtualMemory();
	
	
	//lista wszystkich procesow
	public static List<PCB> processes_List= new LinkedList<PCB>();

	
	//wypisz liste wszystkich procesow
	public void WriteProcessesList()
	{
		for(PCB it: processes_List)
		{
			System.out.println(it.printProcess());
		}
	}

	//glowny proces startowany z poczatkiem systemu
	public PCB main_process;

	public PCB_management()
	{

		//main_process = new PCB("","init",1);

		//ustawienie na ready
		//main_process.setP_state(1);

		//dodanie "init" do listy wszystkich procesow
		//processes_List.add(main_process);


	}
	
	public void new_process(String filename,String p_name) throws FileException
	{
		PCB nowy_proces= new PCB(p_name,filename,p_id_counter);
		processes_List.add(nowy_proces);
		int val = FileSystem.open(filename);
		int i=0;
		byte [] readBytes = FileSystem.readfromFile(val, 1024);
		try
		{


			System.out.println(new String(readBytes));
			nowy_proces.setP_size_file(readBytes.length);
			for(byte abyte : FileSystem.readfromFile(val, 1024))
			{
				readBytes[i++] = abyte;
			}
			
			
		}catch(Exception ignored) {}
		Vector<Byte> B = new Vector<>();
		for (int i2 = 0; i2 < readBytes.length; i2++)
		{
			B.add( Byte.valueOf(readBytes[i2]) );
		}


		vm.createProcess(nowy_proces.getP_id(),B);
		p_id_counter++;
		FileSystem.close(val);
	}
	//fork
	public PCB fork(PCB parent)
	{
		PCB child= new PCB(parent.getF_name(),parent.getP_name(),parent.getP_id());
		

		//dodanie dziecka do listy wszystkich procesow
		processes_List.add(child);

		//dodanie procesu do listy proces�w potomnych
		parent.childrenList.add(child);

		return child;
	}

	public void kill(PCB process_to_kill)
	{
		process_to_kill.setP_state(4);//ustawienie stanu na terminated

		for(PCB it: process_to_kill.childrenList)
		{
			it.setP_id(0);
		}

		Iterator<PCB> iter= processes_List.iterator();
		
		while( iter.hasNext() )
		{
			PCB pr= iter.next();
			if( pr.getP_id() == process_to_kill.getP_id())
			{
				iter.remove();
				vm.removeProcess(process_to_kill.getP_id());
			}
		}

		

	}
}