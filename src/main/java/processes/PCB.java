package processes;

import java.util.LinkedList;
import java.util.List;

public class PCB {
	/* 
  	Blok kontrolny procesu
	*/
	
//id_procesu	
private int p_id;
//id_dziecka_procesu
private int p_children_id;
//lista dzieci procesu
List<PCB> childrenList= new LinkedList<PCB>();
//stany_procesu
/*
 * 1- ready
 * 2- running
 * 3- waiting
 * 4- terminated
 */
private int p_state; 
//rejestr_A
private int p_register_A; 
//rejestr_B
private int p_register_B; 
//rejestr_C
private int p_register_C; 
//rejestr_D
private int p_register_D; 
//licznik_rozkazow
private byte order_counter;
//nazwa_procesu
private String p_name;
//rozmiar_pliku
private int p_size_file;
//czas wykonywania procesu
private int p_virtual_runtime;
//otwarty plik
private int file_ref;
//nazwa pliku
private String f_name;
//zmienna pomagajaca nadawac id
int p_id_counter = 0;


//przyjmuje nazwe procesu, id_procesu, minimalny czas wykonywania procesu, wielkosc procesu
public PCB(String p_name, int p_id, int min_vruntime, byte size,int codelength)
{
	
	this.p_id = p_id;
	this.p_name = p_name;
	p_state= 1;
	
	p_register_A= 0;
	p_register_B= 0;
	p_register_C= 0;
	p_register_D= 0;
	order_counter= size;

	
	
	p_virtual_runtime= min_vruntime;

	
}
public PCB(String filename, String pname)
{
	this.f_name = filename;
	this.p_name = pname;
	
	p_register_A=0;
	p_register_B= 0;
	p_register_C= 0;
	p_register_D= 0;
	order_counter=0;
	file_ref = -1;
	this.p_id= p_id_counter;
	p_id_counter++;
}




	
	//argumentami sa nazwa pliku, nazwa procesu, id procesu
	public PCB(String filename, String pname, int id)
	{
		this.setF_name(filename);
		this.p_name = pname;

		p_state=1;
		p_register_A=0;
		p_register_B= 0;
		p_register_C= 0;
		p_register_D= 0;
		order_counter=0;

		this.p_id= id;
	}

	public int getP_id() {
		return p_id;
	}

	//ustaw id
	public void setP_id(int p_id) {
		this.p_id = p_id;
	}

	public int getP_state() {
		return p_state;
	}

	//ustaw stan
	public void setP_state(int p_state) {
		this.p_state = p_state;
	}


	public byte getOrder_counter() {
		return order_counter;
	}

	public void setOrder_counter(byte order_counter) {
		this.order_counter = order_counter;
	}

	public int getP_register_A() {
		return p_register_A;
	}
	
	//ustaw rejestr A
	public void setP_register_A(int p_register_A) {
		this.p_register_A = p_register_A;
	}

	public int getP_register_B() {
		return p_register_B;
	}
	
	//ustaw rejestr B
	public void setP_register_B(int p_register_B) {
		this.p_register_B = p_register_B;
	}

	public String getP_name() {
		return p_name;
	}
	
	//ustaw nazwe procesu
	public void setP_name(String p_name) {
		this.p_name = p_name;
	}

	public int getP_children_id() {
		return p_children_id;
	}
	
	//ustaw id dziecka procesu
	public void setP_children_id(int p_children_id) {
		this.p_children_id = p_children_id;
	}

	public int getP_virtual_runtime() {
		return p_virtual_runtime;
	}

	//ustaw virtual runtime procesu
	public void setP_virtual_runtime(int p_virtual_runtime) {
		this.p_virtual_runtime = p_virtual_runtime;
	}

	public String printProcess()
	{
		return this.getP_name()+"\t"+this.getP_id()+"\t"+this.getP_state();
	}
	
	//drukuje liste dzieci procesu
	public void writeChildrenlist()
	{
		for(PCB it: this.childrenList)
		{
			System.out.println(it.printProcess());
		}
	}

	public int getP_register_C() {
		return p_register_C;
	}

	//ustaw rejestr C
	public void setP_register_C(int p_register_C) {
		this.p_register_C = p_register_C;
	}

	public int getP_register_D() {
		return p_register_D;
	}

	//ustaw rejestr D
	public void setP_register_D(int p_register_D) {
		this.p_register_D = p_register_D;
	}

	public void setP_file_ref(int P_file_ref) {this.file_ref = P_file_ref;}

	public int getP_file_ref() {return this.file_ref;}


	public int getP_size_file() {
		return p_size_file;
	}

	public void setP_size_file(int p_size_file) {
		this.p_size_file = p_size_file;
	}

	public String getF_name() {
		return f_name;
	}

	public void setF_name(String f_name) {
		this.f_name = f_name;
	}

}