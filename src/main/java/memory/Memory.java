package memory;

import virtualmemory.VirtualMemory;

import java.util.Vector;

public class Memory {
    private static byte[] RAM = new byte[256];

    /**
     * Writes data connected to given frame
     * @param data
     * @param frame
     * @return 
     */
    public static boolean write(Vector<Byte> data, int frame) {
        // check if frame number is correct
        if (frame < 0 || frame > 15) {
            return false;
        }      
        for (int i = 0; i < data.size(); i++) {
            RAM[frame * 16 + i] = data.get(i);
        }
        return true;
    }

    /**
     * Read data from given frame
     * @param frame
     * @return Vector<Byte> with data
     */
    public static Vector<Byte> readFrame(int frame) {
        Vector<Byte> data = new Vector<Byte>();
        try {
            for (int i = 0; i < 16; i++) {
                data.add(RAM[frame * 16 + i]);
            }
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return data;
    }


    /**
     * Writes data connected to the processID
     * @param data
     * @param processID
     * @param address 
     */
    public static void write(byte data, int processID, byte address) {
        int page = address / 16;
        int offset = address - address / 16 * 16;
        int frame = VirtualMemory.getFrame(processID, page);          //uncomment this to enable module
        RAM[frame * 16 + offset] = data;                              //uncomment this to enable module
    }

    /**
     * Writes data directly to memory from given  address
     * @param data
     * @param address 
     */
    public static void write(byte data, int address) {
        try {
            RAM[address] = data;
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Read data connected to the processID from given address
     * @param processID
     * @param address
     * @return 
     */
    public static byte read(int processID, byte address) {
        int page = address / 16;
        int offset = address - address / 16 * 16;
        int frame = VirtualMemory.getFrame(processID,page);
        return RAM[frame * 16 + offset];
    }

    /**
     * Reads data directly from memory from given address
     * @param address
     * @return 
     */
    public static byte read(int address) {
        try {
            return RAM[address];
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            return 0;
        }
    }
    
    /**
     * Return raw RAW
     * @return array of byte
     */
    public static byte[] getRawRAM()
    {
        return RAM;
    }
}