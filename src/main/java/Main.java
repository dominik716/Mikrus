import shell.*;
import files.*;
import memory.*;
import virtualmemory.*;
import processes.*;
import locks.*;


import java.io.File;
import java.io.IOException;

/**
 *
 * @author Adamus i spółka
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, FileException {

        Shell os = new Shell();
        os.boot();
    }
}
