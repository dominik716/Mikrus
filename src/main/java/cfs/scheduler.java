package cfs;
import processes.*;
import interpreter.*;

import java.util.ArrayList;
import java.util.LinkedList;

import files.FileException;

import java.util.Collections;

public class scheduler {
	public static ArrayList<PCB> readyProcesses = new ArrayList<PCB>();
	public static ArrayList<PCB> waitingProcesses = new ArrayList<PCB>();
	Interpreter it = new Interpreter();
	
	//ile rozkazow jest w jednej sesji
	private int targetLatency;
	private int minimumGranulity;
	
	public scheduler(){
		targetLatency = 20;
		minimumGranulity = 5;
	}
	
	void addPcb(PCB p) {
		readyProcesses.add(p);
	}
	
	void show() {
		for(int i=0; i<readyProcesses.size(); i++) {
			System.out.println(readyProcesses.get(i).getP_virtual_runtime());
		}
	}
	
	//bubble sort to sort processes by vr_runtime - orders to make to complete process
	//and clearing list of already done processes
	void schedule() {
		//cleaning lists to not add processes which are terminated and erase them
		readyProcesses.clear();
		waitingProcesses.clear();

		//deleting terminated processes
		LinkedList<PCB> temp = new LinkedList<PCB>();
		for(int i=0; i<PCB_management.processes_List.size(); i++){
			if(PCB_management.processes_List.get(i).getP_state() != 4){
				temp.add(PCB_management.processes_List.get(i));
			}
		}
		PCB_management.processes_List.clear();
		PCB_management.processes_List = temp;
		
		for(int i=0; i<PCB_management.processes_List.size(); i++) {
			if(PCB_management.processes_List.get(i).getP_state() == 1) {
				readyProcesses.add(PCB_management.processes_List.get(i));
			}else if(PCB_management.processes_List.get(i).getP_state() == 3) {
				waitingProcesses.add(PCB_management.processes_List.get(i));
			}
		}
		
		//ordering ready processes
		for(int i=0; i<readyProcesses.size(); i++) {
			for(int j=0; j<readyProcesses.size()-1; j++) {
				if(readyProcesses.get(j).getP_virtual_runtime() < readyProcesses.get(j+1).getP_virtual_runtime()) {
					Collections.swap(readyProcesses, j, j+1);
				}
			}
		}
	}
	
	//step by step walking through processes list
	public void terminateOnce() {
		
		this.schedule();

		targetLatency = 20;
		int timeForEach = 0;
		if(readyProcesses.size() != 0)
			timeForEach = targetLatency / readyProcesses.size();
		
		//scheduling of processes
		
		
		//time for each process counted based on number of processes
		//if there are more processes that timeForEach is smaller than minimalGranulity then targetLatency is expended that every process 
		//can have equal time which is equal to minimum granulity
		if(readyProcesses.size()>0) {
			if(targetLatency/readyProcesses.size() < minimumGranulity) {
				targetLatency = minimumGranulity * readyProcesses.size();
			}
		}else timeForEach = 0; //prob dummy here
			
		
		//loop which is going through list of processes -O
		for(int k=0; k<readyProcesses.size(); k++) {
			//set state for running
			readyProcesses.get(k).setP_state(2);
			System.out.println(readyProcesses.get(k).getP_id());
			for(int i=0; i<timeForEach; i++) {
				try {
					if(readyProcesses.get(k).getP_state() == 3 || readyProcesses.get(k).getP_state() == 4) break;
					it.interpretcommand();
				} catch (FileException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			//then again for ready to get this process back on readyProcesses list
			readyProcesses.get(k).setP_state(3);
			
			System.out.println("proces nr: " + readyProcesses.get(k).getP_id() + ": " + readyProcesses.get(k).getP_virtual_runtime());
		}
	}
	/*
	//function used to process processes all for once -- chyba nie potrzebne to juz w ogole
		void terminateAll() {
			//ppp
			//test var
			int test=1;
			
			//main loop executed until there is any process ready to be executed on processes list
			do {
				
				targetLatency = 20;
				//scheduling of processes
				this.schedule();
				System.out.println("przejscie nr: " + test);
				
				ArrayList<pcb> temp = new ArrayList<pcb>();
				//check if this process is already done and if yes deleting him from list
				for(int i=0; i<readyProcesses.size(); i++) {
					if(readyProcesses.get(i).counter < readyProcesses.get(i).size) {
						temp.add(readyProcesses.get(i));
					}
				}
				readyProcesses.clear();
				readyProcesses = temp;
				
				//time for each process counted based on number of processes
					int timeForEach;
					
					//if there are more processes that timeForEach is smaller than minimalGranulity then targetLatency is expended that every process 
					//can have equal time which is equal to minimum granulity
					if(readyProcesses.size()>0) {
						if(targetLatency/readyProcesses.size() < minimumGranulity) {
							targetLatency = minimumGranulity * readyProcesses.size();
						}
					}
					
					//if there's only one on list
					if(readyProcesses.size()==1) {
						
						//protection to not make more than is needed
						if(readyProcesses.get(0).counter + targetLatency > readyProcesses.get(0).size) {
							timeForEach = readyProcesses.get(0).size - readyProcesses.get(0).counter;
						}else {
							timeForEach = this.targetLatency;
						}
					}else if (readyProcesses.size()==0) {
						//i suppose here should be dummy guy
						timeForEach = 0;
					}else {
						timeForEach = this.targetLatency / readyProcesses.size();
					}
					
					//loop which is going through list of processes -O
					for(int k=0; k<readyProcesses.size(); k++) {
						
						//if a process has less orders to make than target latency then...
						if(readyProcesses.get(k).counter + timeForEach > readyProcesses.get(k).size) {
							int t = readyProcesses.get(k).size - readyProcesses.get(k).counter;
							for(int j=0; j<t; j++) {
								readyProcesses.get(k).counter++;
							}
							
						}else {
							for(int j=0; j<timeForEach; j++) {
								readyProcesses.get(k).counter++;
							}
						}
						
						//O- and increasing their counters based on completely fair divided time for each one ot hem
						
						System.out.println("proces nr: " + readyProcesses.get(k).id + ": " + readyProcesses.get(k).counter);
					}
				
				test++;
			}
			while(readyProcesses.size()>0);	
		}
		*/
}
