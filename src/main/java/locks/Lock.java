package locks;

import processes.PCB;

import java.util.LinkedList;
import java.util.List;

public class Lock extends Locks
{
	protected List<PCB> pcbQueue = new LinkedList<PCB>();
	
	private String lockName;
	
	private PCB lockOwner;
	
	private boolean locked;
	
	protected Lock(String lockName)
	{
		this.lockName = lockName;
	}
	
	protected String getName() { return this.lockName; }
	
	protected boolean isLocked() { return this.locked; }
	
	protected boolean checkPCB(PCB process) 
	{ 
		if(process.getP_id() == lockOwner.getP_id()) return true;
		else return false;
	}
	
	protected void setLock(PCB process, int type)
	{
		if(type == Locks.Locking)
		{
			locked = true;
			lockOwner = process;
		}
		if(type == Unlocking) 
		{
			locked = false;
		}
	}
 }
