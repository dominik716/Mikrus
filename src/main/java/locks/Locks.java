package locks;

import processes.PCB;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Locks
{
	//sta�e wykorzystywane jako argumenty niekt�rych funkcji
	protected static int Locking = 0;
	protected static int Unlocking = 1;
	private static String checkExist = "This lock doesn't exist.";

	//lista wszystkich zamk�w
	public List<Lock> lockList = new LinkedList<Lock>();


	//zwraca zamek na kt�rym wykonywane s� operacje
	private Lock findLock(String file, int type)
	{
		if(!lockList.isEmpty()) //przeszukuje list� je�li nie jest pusta 
		{
			//przeszukuje list� w poszukiwaniu istniej�cego zamka dla danego pliku
			for(Lock lock : lockList)
			{
				if(lock.getName()==file)
				{
					return lock;
				}
			}

			//je�li zamek nie zosta� znaleziony na li�cie sprawdzany jest typ operacji
			if(type==Locking) //je�li funkcja zosta�a wywo�ana w celu zamkni�cia zamka, dodaje go do listy i jest zwracany przez return
			{
				Lock newLock = new Lock(file);
				lockList.add(newLock);
				return newLock;
			}
			else //je�li funkcja nie zosta�a wywo�ana w celu zamkni�cia zamka, zwraca zamek o nazwie b�d�cej identyfikatorem b��du i nie dodaje go do listy
			{
				Lock newLock = new Lock(checkExist);
				return newLock;
			}
		}
		else // je�li lista jest pusta tworzy nowy zamek i dodaje go do listy
		{
			Lock lock = new Lock(file);
			lockList.add(lock);
			return lock;
		}
	}

	public ArrayList<ArrayList<String>> lslocks()
	{
		ArrayList<ArrayList<String>> printLocks = new ArrayList<ArrayList<String>>();
		ArrayList<String> names = new ArrayList<String>();
		ArrayList<String> modes = new ArrayList<String>();
		for (Lock lock : lockList)
		{
			names.add(lock.getName());
			if(lock.isLocked()) modes.add("true");
			else modes.add("false");
		}
		printLocks.add(names);
		printLocks.add(modes);

		return printLocks;
	}



	//zamykanie zamka
	public void lockL(String file, PCB process)
	{
		Lock lock = findLock(file,Locking);

		//mo�liwe tylko gdy zamek nie jest zamkni�ty
		if(lock.isLocked() != true)
		{
			if(!(lock.pcbQueue.isEmpty()))lock.pcbQueue.remove(0);
			//sprawdzanie czy proces jest w gotowo�ci
			if(process.getP_state()==2) {
				lock.setLock(process, Locking);
				System.out.println("Lock locked" + lock.getName());
			}
			else
			{
				if(!(lock.pcbQueue.isEmpty()))
				{
					nextInQueue(lock);
				}
			}

		}
		else // je�li jest zamkni�ty to dodajemy proces do oczekuj�cych
		{
			//ewentualny komunikat
			wait(process);
			lock.pcbQueue.add(process);
		}


	}

	//otwierania zamka
	public void unlockL(String file, PCB process)
	{
		Lock lock = findLock(file,Unlocking);
		if(!(lock.getName()==checkExist)) // wykonuje si�, je�li zamek istnia�
		{
			if(lock.checkPCB(process)) // tylko proces b�d�cy w�a�cicielem zamka mo�e go otworzy�
			{
				lock.setLock(process, Unlocking);
				System.out.println("Lock unlocked" + lock.getName());

				if(lock.pcbQueue.isEmpty()) // je�li �aden proces nie oczekuje na otwarcie zamka, usuwamy zamek z listy
				{
					lockList.remove(lock);
				}
				else // je�li istnieje proces oczekuj�cy na otwarcie zamka
				{
					nextInQueue(lock);
				}
			}
			else
			{
				//ewentualny komunikat je�eli zamek kt�ry ma zosta� otwarty nie istnieje
			}
		}
		else
		{
			//ewentualny komunikat
		}
	}

	private void nextInQueue(Lock lock)
	{
		signal(lock.pcbQueue.get(0));
		lockL(lock.getName(),lock.pcbQueue.get(0));
	}

	void wait(PCB process)
	{
		process.setP_state(3); // stan waiting
	}

	void signal(PCB process)
	{
		process.setP_state(1);
		//dodanie do listy proces�w w gotowo�ci
	}
}	
