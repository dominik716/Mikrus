LISTA ROZKAZÓW

Przenoszenie wartości:

MV dest src		MOVE			--przenosi wartość z src do dest

Operacje Arytmetyczne:

AD dest src		ADD			--dodaj wartość w src do dest
SB dest src		SUBTRACT		--odejmij wartość w src od dest
ML dest src		MULTIPLY		--pomnóż wartość w src razy dest
DV dest src		DIVIDE			--podziel wartość w src przez dest
IC reg			INCREMENT		--zinkrementuj wartość w rejestrze reg
DC reg			DECREMENT		--zdekrementuj wartość w rejestrze reg

Skoki:

JZ [addr]		JUMP ZERO		--przechodzi pod wybrany adres jeżeli CX = 0
JN [addr]		JUMP NOT ZERO		--przechodzi pod wybrany adres jeżeli CX != 0


Drukowanie:

PR reg			PRINT			--drukuje wskazany rejestr

Zakończenie pracy:

EX			EXIT			--koniec zadania