package interpreter;

import memory.Memory;
import processes.*;
import files.*;
import virtualmemory.*;
import files.*;
import memory.*;
import virtualmemory.*;
import processes.*;
import locks.*;
import cfs.*;

import java.lang.reflect.Array;

/*
DONE: AD, SB, ML, DV, IC, DC, MV,
TODO: JN, JZ, JP, NF, OF, CF, DF
 */

public class Interpreter {
    private int AX, BX, CX, DX, PID;
    private int CommandCounter;
    private int sizefile;
    private int fileref;
    Locks locks;
    FileSystem files;

    public Interpreter(){
        this.AX =0;
        this.BX =0;
        this.CX =0;
        this.DX =0;
        this.PID =0;
        CommandCounter = 0;
        sizefile = 0;
        locks = new Locks();
        fileref= -1;
        files = new FileSystem();
    }

    private  void setRegisters(int AX, int BX, int CX, int DX)
    {
        this.AX = AX;
        this.BX = BX;
        this.CX = CX;
        this.DX = DX;
    }
    private void setCommandCounter(byte CCounter)
    {
        this.CommandCounter = CCounter;
    }
    //sprawdzenie czy zmienna jest liczb�
    private boolean isDigit(String command)
    {
        try
        {
            int number = Integer.parseInt(command);
            return true;
        }
        catch (NumberFormatException nfe)
        {
            return false;
        }
    }



    //pobranie pojedyczego polecenia/warto�ci/adresu z pami�ci
    private  String getCommand() {
        int i=0;
        String command = new String();
        String charFromMem = "";


        byte test = VirtualMemory.getByteAt(this.PID,this.CommandCounter);
        byte []t = {test};
        while (!charFromMem.equals(" ") && this.CommandCounter<this.sizefile)
        //&& VirtualMemory.Order_Table.size()>this.CommandCounter)
        {
            command += charFromMem;
            t[0] = VirtualMemory.getByteAt(this.PID,this.CommandCounter);
            charFromMem = new String(t);

            //System.out.println("byte:" + new String(t));
            //System.out.println("cc:" + this.CommandCounter);
            //System.out.print(charFromMem);
            this.CommandCounter++;
        }
        if(this.CommandCounter==this.sizefile)
        {
            //command += new String(new byte[] {VirtualMemory.getByteAt(this.PID,this.CommandCounter-1)});
        }
        return command;

    }



    //sprawdzenie kt�ry proces jest w stanie "running" i zwr�cenie jego indeksu w li�cie
    private int findRunningProcess()
    {
        for (int i = 0; i < scheduler.readyProcesses.size(); i++) {
            if (scheduler.readyProcesses.get(i).getP_state()== 2) {
                this.PID = scheduler.readyProcesses.get(i).getP_id();
                this.sizefile = scheduler.readyProcesses.get(i).getP_size_file();
                return i;
            }
        }
        return -1;
    }
    private void arithmeticOp(String command, String type, String command2)
    {
        int val = 0;
        val = getVal(command2);
        if (type.equals("+"))
        {
            if (command.equals("AX")) { this.AX += val; }
            else if (command.equals("BX"))  { this.BX+= val; }
            else if (command.equals("CX")) { this.CX+= val; }
            else if (command.equals("DX")) { this.DX+= val; }
        }
        else if (type.equals("-"))
        {
            if (command.equals("AX")) { this.AX -= val; }
            else if (command.equals("BX"))  { this.BX -= val; }
            else if (command.equals("CX")) { this.CX -= val; }
            else if (command.equals("DX")) { this.DX -= val; }
        }
        else if (type.equals("*"))
        {
            if (command.equals("AX")) { this.AX *= val; }
            else if (command.equals("BX"))  { this.BX *= val; }
            else if (command.equals("CX")) { this.CX *= val; }
            else if (command.equals("DX")) { this.DX *= val; }
        }
        else if (type.equals("/"))
        {
            if (command.equals("AX")) { this.AX /= val; }
            else if (command.equals("BX"))  { this.BX /= val; }
            else if (command.equals("CX")) { this.CX /= val; }
            else if (command.equals("DX")) { this.DX /= val; }
        }
    }
    private int getVal(String command)
    {
        if (command.equals("AX")) { return this.AX; }
        else if (command.equals("BX"))  { return this.BX; }
        else if (command.equals("CX")) { return this.CX; }
        else if (command.equals("DX")) { return this.DX; }
        else if (isDigit(command) == true) { return Integer.parseInt(command); }
        else if (command.charAt(0)=='[') { return getValFromAddress(command); }
        else return 0;
    }
    private int getValFromAddress(String address)
    {
        int addressOfNumber = Integer.parseInt(address.substring(1, address.length() - 1));
        System.out.println(addressOfNumber);
        int numOfBytes = Integer.parseInt(" ");
        byte [] numberFromMemory = new byte [numOfBytes];
        for(int i = 0; i<numOfBytes; i++)
        {
            numberFromMemory[i]= VirtualMemory.getByteAt(this.PID,addressOfNumber);  //potrzeba funkcji pobierania poj. znaku
            addressOfNumber++;
        }
        return Integer.parseInt(numberFromMemory.toString());
    }
    private String getName(String command)
    {
        return command.substring(1,command.length()-1);
    }



    // g��wna funkcja wywo�ywana przez Shell
    //wykonanie pojedynczego rozkazu
    public int interpretcommand() throws FileException {
        int rp = findRunningProcess();
        setCommandCounter(scheduler.readyProcesses.get(rp).getOrder_counter());
        setRegisters(scheduler.readyProcesses.get(rp).getP_register_A(), scheduler.readyProcesses.get(rp).getP_register_B(),
                scheduler.readyProcesses.get(rp).getP_register_C(), scheduler.readyProcesses.get(rp).getP_register_D());
        fileref = scheduler.readyProcesses.get(rp).getP_file_ref();
        String command = getCommand();
        //suma
        if(command.equals("AD"))
        {
            command = getCommand();
            String command2 = getCommand();
            arithmeticOp(command, "+",command2);
        }
        //r�nica
        else if(command.equals("SB"))
        {
            command = getCommand();
            String command2 = getCommand();
            arithmeticOp(command, "-",command2);
        }
        //iloczyn
        else if(command.equals("ML"))
        {
            command = getCommand();
            String command2 = getCommand();
            arithmeticOp(command, "*",command2);
        }
        //iloraz
        else if(command.equals("DV"))
        {
            command = getCommand();
            String command2 = getCommand();
            arithmeticOp(command, "/",command2);
        }
        //inkrementacja
        else if (command.equals("IC")) {
            command = getCommand();
            if (command.equals("AX")) {
                this.AX++;
            } else if (command.equals("BX")) {
                this.BX++;
            } else if (command.equals("CX")) {
                this.CX++;
            } else if (command.equals("DX")) {
                this.DX++;
            }
        }
        //dekrementacja
        else if (command.equals("DC")) {
            command = getCommand();
            if (command.equals("AX")) {
                this.AX--;
            } else if (command.equals("BX")) {
                this.BX--;
            } else if (command.equals("CX")) {
                this.CX--;
            } else if (command.equals("DX")) {
                this.DX--;
            }
        }
        //przenoszenie warto�ci do rejestru
        else if(command.equals("MV"))
        {
            command = getCommand();
            int val = getVal(getCommand());
            if (command.equals("AX")) { this.AX = val; }
            else if (command.equals("BX"))  { this.BX = val; }
            else if (command.equals("CX")) { this.CX = val; }
            else if (command.equals("DX")) { this.DX = val; }
        }
        //skok bezwarunkowy
        else if(command.equals("JP"))
        {
            String address = getCommand();
            this.CommandCounter = Integer.parseInt(address.substring(1, address.length() - 1));
        }
        //skok je�eli CX == 0
        else if(command.equals("JZ"))
        {
            if (this.CX == 0)
            {
                String address = getCommand();
                this.CommandCounter = Integer.parseInt(address.substring(1, address.length() - 1));
            }
        }
        //skok je�eli CX != 0
        else if(command.equals("JN"))
        {
            if (this.CX != 0)
            {
                String address = getCommand();
                this.CommandCounter = Integer.parseInt(address.substring(1, address.length() - 1));
            }
        }
        //tworzenie pliku
        else if (command.equals("NF"))
        {
            files.createFile(getCommand());
        }
        else if (command.equals("OF"))
        {
            command = getCommand();
            locks.lockL(command,scheduler.readyProcesses.get(rp));
            if (scheduler.readyProcesses.get(rp).getP_state()==3)
            {
                scheduler.readyProcesses.get(rp).setP_virtual_runtime(scheduler.readyProcesses.get(rp).getP_virtual_runtime()-1);
                this.CommandCounter = this.CommandCounter - command.length() - 4;
            }
            else {
                scheduler.readyProcesses.get(rp).setP_file_ref(files.open(command));
            }
        }
        else if (command.equals("AF"))
        {
            command = getCommand();
            boolean i = false;
            byte temp = 0;
            if (command.equals("AX")) {temp = (byte)this.AX;}
            else if (command.equals("BX")) {temp = (byte)this.BX;}
            else if (command.equals("CX")) {temp = (byte)this.CX;}
            else if (command.equals("DX")) {temp = (byte)this.DX;}
            else if (command.charAt(0)=='[') {temp =(byte)getValFromAddress(command);}
            else {
                System.out.print(command.substring(1,command.length()-1));
                files.appendFile(this.fileref,command.substring(1,command.length()-1).getBytes());
                i = true;}
            if (!i){
                byte[] data = new byte[1];
                data[0] = temp;
                System.out.print(temp);
                this.files.appendFile(this.fileref,data);
            }

        }
        //zamknięcie pliku
        else if (command.equals("CF"))
        {
            this.files.close(this.fileref);
            locks.unlockL(getCommand(),scheduler.readyProcesses.get(rp));
            this.fileref = -1;
        }

        else if (command.equals("EQ"))
        {
            int val1 = getVal(getCommand());
            int val2 = getVal(getCommand());
            int val3 = (val1==val2)? 1:0;
            System.out.println(val3);
            command = getCommand();
            System.out.println(command);
            if (command.equals("AX"))
            {
                this.AX = val3;
            }
            else if (command.equals("BX"))
            {
                this.BX = val3;
            }
            else if (command.equals("CX"))
            {
                this.CX = val3;
            }
            else if (command.equals("DX"))
            {
                this.DX = val3;
            }

        }


        else if (command.equals("PR"))
        {
            command = getCommand();
            if (command.equals("AX")) {
                System.out.println("Warto�� rejestru AX: "+this.AX);
            } else if (command.equals("BX")) {
                System.out.println("Warto�� rejestru BX: "+this.BX);
            } else if (command.equals("CX")) {
                System.out.println("Warto�� rejestru CX: "+this.CX);
            } else if (command.equals("DX")) {
                System.out.println("Warto�� rejestru DX: "+this.DX);
            }
        }
        else if (command.equals("CP"))
        {
            //tworzenie dziecka, forkowanie aktualnego procesu, nie mam dost�pu do forka
            // nwm czy w og�le potrzebne w sumie xD
        }
        //zako�czenie programu
        else if(command.equals("EX"))
        {
            //zabijanie procesu - wykona� si� - nie mog� wywo�a� funkcji kill poniewa�
            // nie da sie odwo�a� do procesu w linked list
            scheduler.readyProcesses.get(rp).setOrder_counter((byte)this.CommandCounter);
            scheduler.readyProcesses.get(rp).setP_state(4);
            System.out.println("Terminating process");
            return 4;
        }
        //pod koniec wykonwywania rozkazu, aktualizuje pcb procesu oraz zwraca 2 = running
        scheduler.readyProcesses.get(rp).setOrder_counter((byte)this.CommandCounter);
        scheduler.readyProcesses.get(rp).setP_register_A(this.AX);
        scheduler.readyProcesses.get(rp).setP_register_B(this.BX);
        scheduler.readyProcesses.get(rp).setP_register_C(this.CX);
        scheduler.readyProcesses.get(rp).setP_register_D(this.DX);
        scheduler.readyProcesses.get(rp).setP_virtual_runtime(scheduler.readyProcesses.get(rp).getP_virtual_runtime()+1);

        System.out.println("Registers:");
        System.out.println(this.AX);
        System.out.println(this.BX);
        System.out.println(this.CX);
        System.out.println(this.DX);

        return 2;
    }
}