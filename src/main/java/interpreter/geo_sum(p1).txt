Wstępny wygląd sumy ciągu geometrycznego:
do rejestru AX wprowadzany jest pierwszy wyraz,
do rejestru CX wprowadzana jest liczba wykonań,
do rejestru DX wprowadzane jest q.
Wynik zapisywany jest w nowo utworzonym i otwartym pliku "ans".

(0)	    MV AX 8
(8)	    MV BX 8
(16)	MV CX 4
(24)	MV DX 2
        CF  "o1"
        OF "o1"
(32)	JZ [69]
        WF BX
        WF " "
(40)	ML AX DX
(49)	AD BX AX
(58)	DC CX
(64)	JP [32]
        CF "o1"
(72)	PR BX
(78)    EX