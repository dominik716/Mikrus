package files;
import java.util.*;

//KATALOG DWUPOZIOMOWY JEDNAK A NIE DRZEWO
public class FileSystem {
	static int currentdir;
	public static Hashtable<String,Catalog_Entry> rootdirectory;
	public static Vector< Hashtable<String, Catalog_Entry>> directories;
	public static INODE[]  fcblist; //lista wszystkich i_wezlow
	public static Disk disk;
	public static INODE[] openfiles;//systemowa tablica otwartych plikow
	public static Vector <Integer> process_openfiles; //procesowa tablica otwartych plik�w


	public FileSystem() {
		rootdirectory = new Hashtable<String,Catalog_Entry>();
		directories = new Vector<Hashtable<String, Catalog_Entry>> ();
		directories.add(rootdirectory);
		currentdir = directories.indexOf(rootdirectory); //zaczyna od rootdirectory
		fcblist = new INODE[128]; //maks tyle plikow ile blokow dyskowych
		disk = new Disk();
		openfiles = new INODE[128];
		process_openfiles = new Vector<Integer>(); //nie przekroczy [32] ale moze byc vector bo wielkosc kontroluje openfiles

	}

	private static int freefcbindex() {
		for(int i = 0; i<fcblist.length; i++)
			if(fcblist[i] == null)
				return i;
		return -1;
	}

	private static int openfilesindex() {
		for(int i = 0; i<openfiles.length; i++)
			if(openfiles[i] == null)
				return i;
		return -1;
	}

	public static boolean createFile(String name) throws FileException {

		if(directories.get(currentdir).containsKey(name)) {
			return false;
		}
		int pom = 0;

		INODE inode = new INODE();
		inode.size = 0;
		inode.block_index1 = disk.assign_freeblock();
		if(inode.block_index1 == -1) throw new FileException ("Brak pamieci");
		inode.block_index2 = -1;
		for(int i=0;i<3;i++) {
			inode.index_block[i] = -1;}
		Date date = new Date();
		inode.i_ctime = date.getTime();
		inode.i_mtime = inode.i_ctime;
		inode.pointer = inode.block_index1*128;
		pom = freefcbindex();
		fcblist[pom] = inode;

		Catalog_Entry entry = new Catalog_Entry();
		entry.name = name;
		entry.type = 0;
		entry.fcb_index = pom;
		directories.get(currentdir).put(name,entry);

		System.out.println("Stworzono plik o nazwie " + name);
		return true;
	}


	public boolean createDirectory (String name) {
		if(directories.get(currentdir).containsKey(name)) {
			return false;
		}
		Catalog_Entry entry = new Catalog_Entry();
		entry.name = name;
		entry.type = 1;
		entry.fcb_index = -1;
		directories.get(currentdir).put(name,entry);
		Hashtable<String, Catalog_Entry> catalog = new Hashtable<String, Catalog_Entry>();
		directories.add(catalog);
		return true;
	}
	public void listDir() {
		Collection<Catalog_Entry> pom = directories.get(currentdir).values();
		Catalog_Entry[] pom2 = pom.toArray(new Catalog_Entry[directories.get(currentdir).size()]);
		if(currentdir == 0) {
			System.out.println("Pliki:");
			for(int i=0; i<directories.get(currentdir).size(); i++) {
				if(pom2[i].type == 0) System.out.println(pom2[i].name);
			}
			System.out.println("Podkatalogi:");
			for(int i=0; i<directories.get(currentdir).size(); i++) {
				if(pom2[i].type == 1) System.out.println(pom2[i].name);
			}}
		else {
			System.out.println("Pliki:");
			for(int i=0; i<directories.get(currentdir).size(); i++) {
				if(pom2[i].type == 0) System.out.println(pom2[i].name);
			}
		}}



	public void changeDir(String name) {
		int filecontrol = 0;
		if(currentdir == 0) {
			Collection<Catalog_Entry> pom = directories.get(currentdir).values();
			Catalog_Entry[] pom2 = pom.toArray(new Catalog_Entry[directories.get(currentdir).size()]);
			for(int i=0; i<directories.get(currentdir).size(); i++) {

				if(pom2[i].type == 0) { filecontrol++;
					System.out.println("!!pom2[i].name:" + pom2[i].name);
				}
				if(pom2[i].name == name && pom2[i].type == 1) {
					currentdir = (i-filecontrol+1);
				}
			}}
		else currentdir = 0;
	}

	public static int open(String name)throws FileException {
		if(directories.get(currentdir).containsKey(name)) {
			for(int i=0; i<process_openfiles.size();i++) {
				if(openfiles[process_openfiles.get(i)] == fcblist[directories.get(currentdir).get(name).fcb_index]) {
					throw new FileException("Ten plik juz jest otwarty");
				}}

			int freeindex = openfilesindex();
			if(freeindex == -1)throw new FileException("Brak wolnej pamieci");
			openfiles[freeindex] = fcblist[directories.get(currentdir).get(name).fcb_index];
			process_openfiles.add(freeindex);
			System.out.println("Otwarto plik: " + name);
			return process_openfiles.indexOf(freeindex);
		}
		else return -1;

	}

	public static void close(int index) {
		int pom = process_openfiles.get(index);
		openfiles[pom] = null;
		process_openfiles.remove(index);

	}

	//tu mi sie pojebalo i zapomnialam ze pointer wgl istnieje wiec tak nas sile go wstawilam:)
	//tak samo przy appendFile i readfile bozeeeee ten wskaznik jest kompletnie bezuzyteczny w tym wypadku:)
	//nie chce mi sie tego na sile u�ywac jak bed� miala nadmiar czasu to to zmienie jak nie to nie
	public boolean writetoFile(int index, byte[] data) throws FileException {
		if(!process_openfiles.contains(index)) {
			return false;
		}
		int pom = process_openfiles.get(index);
		clearFile(openfiles[pom]); //czy�ci ca�y plik zanim znowu do niego zapisze
		int i = 0;
		openfiles[pom].size = i;
		openfiles[pom].pointer = openfiles[pom].block_index1 * 128;

		i = disk.writetoblock(openfiles[pom].block_index1, 0, data);
		openfiles[pom].pointer += i;
		if(i<data.length) {
			if(openfiles[pom].block_index2 == -1) {
				openfiles[pom].block_index2 = disk.assign_freeblock();
			}

			openfiles[pom].pointer = openfiles[pom].block_index2 * 128;
			i = disk.writetoblock(openfiles[pom].block_index2,i,data);
			openfiles[pom].pointer += (i-128);
		}
		for(int c = 0; c <3; c++) {
			if(i< data.length) {
				if(openfiles[pom].index_block[c] == -1) {
					openfiles[pom].index_block[c] = disk.assign_freeblock();
				}
				openfiles[pom].pointer = openfiles[pom].index_block[c] * 128;
				i = disk.writetoblock(openfiles[pom].index_block[c],i,data);
				openfiles[pom].pointer += (i-(64+(128*c)));
			}
		}

		openfiles[pom].size += i;
		Date date = new Date();
		openfiles[pom].i_mtime = date.getTime();
		return true;
	}


	public boolean appendFile(int index, byte[] data) throws FileException {
		if(!process_openfiles.contains(index)) {
			return false;
		}
		int pom = process_openfiles.get(index);
		int i = 0;
		if(disk.freeblockspace(openfiles[pom].block_index1) != -1) {

			openfiles[pom].pointer = disk.freeblockspace(openfiles[pom].block_index1);
			i = disk.appendblock(openfiles[pom].block_index1, data);
			//jak blok nie jest zapisany do konca to do niego dopisuje
			openfiles[pom].pointer += i;
			if(i < data.length) {

				openfiles[pom].block_index2 = disk.assign_freeblock();
				i = disk.writetoblock(openfiles[pom].block_index2,i, data);
				openfiles[pom].pointer = disk.freeblockspace(openfiles[pom].block_index2);
			}
			if(i<data.length) {
				openfiles[pom].index_block[0] = disk.assign_freeblock();
				i = disk.writetoblock(openfiles[pom].index_block[0],i, data);
				openfiles[pom].pointer = disk.freeblockspace(openfiles[pom].index_block[0]);
			}
			if(i<data.length) {
				openfiles[pom].index_block[1] = disk.assign_freeblock();
				i = disk.writetoblock(openfiles[pom].index_block[1],i, data);
				openfiles[pom].pointer = disk.freeblockspace(openfiles[pom].index_block[1]);
			}
			if(i<data.length) {
				openfiles[pom].index_block[2] = disk.assign_freeblock();
				i = disk.writetoblock(openfiles[pom].index_block[2],i, data);
				openfiles[pom].pointer = disk.freeblockspace(openfiles[pom].index_block[2]);
			}
			openfiles[pom].size += i;
			Date date = new Date();
			openfiles[pom].i_mtime = date.getTime();
			return true;
		}
		else if(openfiles[pom].block_index2 != -1 && disk.freeblockspace(openfiles[pom].block_index2) != -1) {

			openfiles[pom].pointer = disk.freeblockspace(openfiles[pom].block_index2);
			i = disk.appendblock(openfiles[pom].block_index2, data);

			openfiles[pom].pointer += i;

			if(i<data.length) {
				openfiles[pom].index_block[0] = disk.assign_freeblock();
				i = disk.writetoblock(openfiles[pom].index_block[0],i, data);
				openfiles[pom].pointer = disk.freeblockspace(openfiles[pom].index_block[0]);
			}
			if(i<data.length) {
				openfiles[pom].index_block[1] = disk.assign_freeblock();
				i = disk.writetoblock(openfiles[pom].index_block[1],i, data);
				openfiles[pom].pointer = disk.freeblockspace(openfiles[pom].index_block[1]);
			}
			if(i<data.length) {
				openfiles[pom].index_block[2] = disk.assign_freeblock();
				i = disk.writetoblock(openfiles[pom].index_block[2],i, data);
				openfiles[pom].pointer = disk.freeblockspace(openfiles[pom].index_block[2]);
			}
			openfiles[pom].size += i;
			Date date = new Date();
			openfiles[pom].i_mtime = date.getTime();
			return true;
		}
		else if(openfiles[pom].block_index2 == -1) {
			openfiles[pom].block_index2 = disk.assign_freeblock();
			openfiles[pom].pointer = openfiles[pom].block_index2*128;
			i = disk.writetoblock(openfiles[pom].block_index2,i, data);
			openfiles[pom].pointer += i;

			if(i<data.length) {
				openfiles[pom].index_block[0] = disk.assign_freeblock();
				i = disk.writetoblock(openfiles[pom].index_block[0],i, data);
				openfiles[pom].pointer = disk.freeblockspace(openfiles[pom].index_block[0]);
			}
			if(i<data.length) {
				openfiles[pom].index_block[1] = disk.assign_freeblock();
				i = disk.writetoblock(openfiles[pom].index_block[1],i, data);
				openfiles[pom].pointer = disk.freeblockspace(openfiles[pom].index_block[1]);
			}
			if(i<data.length) {
				openfiles[pom].index_block[2] = disk.assign_freeblock();
				i = disk.writetoblock(openfiles[pom].index_block[2],i, data);
				openfiles[pom].pointer = disk.freeblockspace(openfiles[pom].index_block[2]);
			}
			openfiles[pom].size += i;
			Date date = new Date();
			openfiles[pom].i_mtime = date.getTime();
			return true;
		}
		else if(openfiles[pom].index_block[0] != -1 && disk.freeblockspace(openfiles[pom].index_block[0]) != -1) {

			openfiles[pom].pointer = disk.freeblockspace(openfiles[pom].index_block[0]);
			i = disk.appendblock(openfiles[pom].index_block[0], data);

			openfiles[pom].pointer += i;
			if(i<data.length) {
				openfiles[pom].index_block[1] = disk.assign_freeblock();
				i = disk.writetoblock(openfiles[pom].index_block[1],i, data);

				openfiles[pom].pointer = disk.freeblockspace(openfiles[pom].index_block[1]);
			}
			if(i<data.length) {
				openfiles[pom].index_block[2] = disk.assign_freeblock();
				i = disk.writetoblock(openfiles[pom].index_block[2],i, data);

				openfiles[pom].pointer = disk.freeblockspace(openfiles[pom].index_block[2]);
			}
			openfiles[pom].size += i;
			Date date = new Date();
			openfiles[pom].i_mtime = date.getTime();
			return true;
		}
		else if(openfiles[pom].index_block[0] == -1) {
			openfiles[pom].index_block[0] = disk.assign_freeblock();
			openfiles[pom].pointer = openfiles[pom].index_block[0]*128;
			i = disk.writetoblock(openfiles[pom].index_block[0],i, data);
			openfiles[pom].pointer += i;
			if(i<data.length) {
				openfiles[pom].index_block[1] = disk.assign_freeblock();
				i = disk.writetoblock(openfiles[pom].index_block[1],i, data);
				openfiles[pom].pointer = disk.freeblockspace(openfiles[pom].index_block[1]);
			}
			if(i<data.length) {
				openfiles[pom].index_block[2] = disk.assign_freeblock();
				i = disk.writetoblock(openfiles[pom].index_block[2],i, data);
				openfiles[pom].pointer = disk.freeblockspace(openfiles[pom].index_block[2]);
			}
			openfiles[pom].size += i;
			Date date = new Date();
			openfiles[pom].i_mtime = date.getTime();
			return true;
		}
		else if(openfiles[pom].index_block[1] != -1 && disk.freeblockspace(openfiles[pom].index_block[1]) != -1) {
			openfiles[pom].pointer = disk.freeblockspace(openfiles[pom].index_block[1]);
			i = disk.appendblock(openfiles[pom].index_block[1], data);
			openfiles[pom].pointer += i;
			if(i<data.length) {
				openfiles[pom].index_block[2] = disk.assign_freeblock();
				i = disk.writetoblock(openfiles[pom].index_block[2],i, data);
				openfiles[pom].pointer = disk.freeblockspace(openfiles[pom].index_block[2]);
			}
			Date date = new Date();
			openfiles[pom].i_mtime = date.getTime();
			return true;
		}
		else if(openfiles[pom].index_block[1] == -1) {
			openfiles[pom].index_block[1] = disk.assign_freeblock();
			openfiles[pom].pointer = openfiles[pom].index_block[1] *128;
			i = disk.writetoblock(openfiles[pom].index_block[1],i, data);
			openfiles[pom].pointer += i;
			if(i<data.length) {
				openfiles[pom].index_block[2] = disk.assign_freeblock();
				i = disk.writetoblock(openfiles[pom].index_block[2],i, data);
				openfiles[pom].pointer = disk.freeblockspace(openfiles[pom].index_block[2]);
			}
			Date date = new Date();
			openfiles[pom].i_mtime = date.getTime();
			return true;
		}
		else if(openfiles[pom].index_block[2] != -1 && disk.freeblockspace(openfiles[pom].index_block[2]) != -1) {
			openfiles[pom].pointer = disk.freeblockspace(openfiles[pom].index_block[2]);
			i = disk.appendblock(openfiles[pom].index_block[2], data);
			openfiles[pom].pointer += i;
			Date date = new Date();
			openfiles[pom].i_mtime = date.getTime();
			return true;
		}
		else if(openfiles[pom].index_block[2] == -1) {
			openfiles[pom].index_block[2] = disk.assign_freeblock();
			openfiles[pom].pointer = 	openfiles[pom].index_block[2] *128;
			i = disk.writetoblock(openfiles[pom].index_block[2],i, data);
			openfiles[pom].pointer += i;
		}
		Date date = new Date();
		openfiles[pom].i_mtime = date.getTime();
		return true;
	}

	public static byte[] readfromFile(int index, int count) throws FileException{
		if(!process_openfiles.contains(index)) {
			throw new FileException("Plik nie jest otwarty");
		}
		byte[] data;
		int pom = process_openfiles.get(index);
		openfiles[pom].pointer = openfiles[pom].block_index1*128;
		if(count > openfiles[pom].size) {
			data = new byte[openfiles[pom].size];
		} else {
			data = new byte[count]; }
		int size_control = 0;
		byte[] pom2;
		pom2 = disk.readblock(openfiles[pom].block_index1, count);
		for(int i = 0; i < pom2.length; i++) {
			data[i] = pom2[i];
			size_control++;
			openfiles[pom].pointer++;
		}
		if(pom2.length < count && openfiles[pom].block_index2 != -1) {
			pom2 = disk.readblock(openfiles[pom].block_index2, (count-size_control));

			for(int i = 0; i < pom2.length; i++) {
				data[size_control] = pom2[i];
				size_control++;
				openfiles[pom].pointer++;
			}
		}
		for(int a = 0; a < 3; a++) {
			if(pom2.length < count && openfiles[pom].index_block[a] != -1) {
				pom2 = disk.readblock(openfiles[pom].index_block[a], count-size_control);
				for(int i = 0; i < pom2.length; i++) {
					data[size_control] = pom2[i];
					size_control++;
					openfiles[pom].pointer++;
				}
			}}
		return data;
	}

	public String bytetoString (byte[] data) {
		String text = new String(data);
		return text;
	}
	public byte[] Stringtobyte(String text) {
		return text.getBytes();
	}
	private static void clearFile(INODE file) {
		for(int i=2; i>=0;i--)
			if(file.index_block[i] != -1) {
				disk.clearblock(file.index_block[i]);
			}
		if(file.block_index2 != -1)
			disk.clearblock(file.block_index2);
		if(file.block_index1 != -1)
			disk.clearblock(file.block_index1);
	}

	public static boolean deleteFile(String name) throws FileException {

		if(directories.get(currentdir).containsKey(name)) {
			for(int i=0; i<process_openfiles.size();i++) {
				if(openfiles[process_openfiles.get(i)] == fcblist[directories.get(currentdir).get(name).fcb_index]) {
					openfiles[process_openfiles.get(i)] = null;
					process_openfiles.remove(i);
				}}
			//sprawdza czy dalej plik jest w systemowej tablichy otwartych plikow (czy jest otwarty przez inny proces)
			//kontynuuje tylko jezeli plik jest zamkniety dla kazdego procesu
			for(int i=0; i<128;i++) {
				if(openfiles[i] == fcblist[directories.get(currentdir).get(name).fcb_index]) {
					throw new FileException("Plik jest otwarty dla innego procesu");
				}}
			clearFile(fcblist[directories.get(currentdir).get(name).fcb_index]);
			fcblist[directories.get(currentdir).get(name).fcb_index].size =0;
			fcblist[directories.get(currentdir).get(name).fcb_index] = null;

			directories.get(currentdir).remove(name);
			return true;
		}
		else return false;
	}
	public boolean deleteDir(String name)throws FileException {
		currentdir = 0;
		if(directories.get(currentdir).containsKey(name) && directories.get(currentdir).get(name).type == 1) {
			changeDir(name);
			Collection<Catalog_Entry> pom = directories.get(currentdir).values();
			Catalog_Entry[] pom2 = pom.toArray(new Catalog_Entry[directories.get(currentdir).size()]);
			for(int i=0; i<directories.get(currentdir).size();i++) {

				try {
					deleteFile(pom2[i].name);
				} catch (FileException e) {
					currentdir = 0;
					throw new FileException("Usuwany katalog zawiera plik otwarty przez inny proces");
				} }
			directories.remove(currentdir);
			currentdir = 0;
			directories.get(currentdir).remove(name);
			return true;
		}
		else return false;
	}

	public static void main(String[] args) {
		FileSystem test = new FileSystem();
		int index =-1;
		int index2=-1;
		try {
			test.createFile("pliczek");
			test.createFile("zaba");
			index = test.open("pliczek");

		} catch (FileException e) {
			System.out.println(e);
		}
		String a = "hello";

		try {
			test.writetoFile(index, test.Stringtobyte(a));
			System.out.println("test " + test.bytetoString(test.readfromFile(index, 20)));
		} catch (FileException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String b = "ajjkarambaaaaaaaaamam aaaaaaaaaaaaaabbbbbbbcccccccccccccccccccccccccddddddddddddd";

		try {
			test.writetoFile(index, test.Stringtobyte(b));
			System.out.println("test " + test.bytetoString(test.readfromFile(index, 80)));
		} catch (FileException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String c = "eeeeeeeeeeeeeeeeeeeeeeeeeeeeffffffffffffffffffffffffffffffffffffffeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeffffffffffffffffffffffffffffffffffffffeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeffffffffffffffffffffffffffffffffffffffeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeffffffffffffffffffffffffffffffffffffffeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeffffffffffffffffffffffffffffffffffffffeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeffffffffffffffffffffffffffffffffffffffeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeffffffffffffffffffffffffffffffffffffff";


		try {test.appendFile(index, test.Stringtobyte(c));
			System.out.println("test " + test.bytetoString(test.readfromFile(index, 170)));
		} catch (FileException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			index2 = test.open("zaba");
		} catch (FileException e) {

			e.printStackTrace();
		}
		test.close(index2);
		try {
			index2 = test.open("zaba");
		} catch (FileException e) {

			e.printStackTrace();
		}
		test.listDir();
		test.listDir();

		test.changeDir("podkatalogtestowy2");
		try {
			test.createFile("plikpodkatalogowy2");
			test.createFile("plikpodkatalogowy3");
		} catch (FileException e) {
			e.printStackTrace();
		} test.listDir();
		test.changeDir("root");
		test.listDir();
		test.changeDir("podkatalogtestowy2");
		test.listDir();
		try {
			test.deleteFile("plikpodkatalogowy2");
		} catch (FileException e) {

			e.printStackTrace();
		}
		test.listDir();
		try {
			test.deleteDir("podkatalogtestowy2");
		} catch (FileException e) {

			e.printStackTrace();
		}
		test.listDir();

		try {test.appendFile(index2, test.Stringtobyte(c));
			System.out.println("test " + test.bytetoString(test.readfromFile(index2, 1024)));
		} catch (FileException e) {

			e.printStackTrace();
		}
		test.close(index2);
		disk.stan_dysku();
	/*try {
		System.out.println("test " + test.bytetoString(test.readfromFile(index2, 170)));
	} catch (FileException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}*/
	}

}