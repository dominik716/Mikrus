package files;

public class FileException extends Exception {
	public FileException(String error){
		super(error);
	}
}
