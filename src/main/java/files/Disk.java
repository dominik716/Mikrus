package files;

public class Disk {
	public static byte[] DISK;
	public static final int blocksize = 128;
	public static int [] bitmap;

	public Disk () {

		DISK = new byte[1024];// to zaklada ze blok ma 32 bajty
		for(int i=0; i< DISK.length; i++)
			DISK[i] = (byte)0;

		bitmap = new int[128]; //taka wielkosc jaka ilosc blokow
		for(int i=0; i< bitmap.length; i++)
			bitmap[i] = 1;
	}
	public int writetoblock(int n, int a, byte[] data) {

		for(int i = n*blocksize; i < (n+1)*blocksize; i++, a++) {
			DISK[i] = data[a];
			if(a+1 >= data.length) {
				return a+1;}
		}
		System.out.println("A : " + a);
		return a;
	}
	public int appendblock(int n, byte[] data) {
		int pom = 0;
		for(int i = n*blocksize; i < (n+1)*blocksize; i++) {
			if(DISK[i] == (byte)0) {
				DISK[i] = data[pom];
				pom++;}
			if(pom+1 >= data.length) return pom;

		}
		return pom;
	}
	public byte[] readblock(int n, int count) {
		byte[] data;
		int pom = 0;

		if(freeblockspace(n)==-1 && count >= 128){
			data = new byte[blocksize];}
		else if(freeblockspace(n) - (n*blocksize) <= count && freeblockspace(n) - (n*blocksize) >= 0) {

			data = new byte[freeblockspace(n)-(n*blocksize) ];}
		else {
			data = new byte[count];}
		for(int i = n*blocksize; pom < data.length; i++, pom++) {
			data[pom] = DISK[i];

			if(pom+1 >= data.length) return data;
		}
		return data;
	}
	public void clearblock(int n) {
		for(int i = n*128; i<(n+1)*128;i++) {
			DISK[i] = (byte)0;
		}
		bitmap[n] = 1;
	}
	public int assign_freeblock() {
		for(int i=0; i<bitmap.length; i++)
			if(bitmap[i] == 1) {
				bitmap[i] = 0;
				return i;}
		return -1;
	}
	public int freeblockspace(int n) { //zwraca pierwszy niezapisany bajt w bloku
		for(int i = n*blocksize; i < (n+1)*blocksize; i++) {
			if(DISK[i] == (byte)0) return i;
		}
		return -1;
	}

	public void stan_dysku() {
		int check = 0;
		for(int i=0; i < 128; i++) {
			if(bitmap[i] == 1) {
				check++;
			}}
		System.out.println("Ilosc wolnych blokow dysku: " + check);
	}
}
